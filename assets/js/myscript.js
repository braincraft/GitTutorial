$(function(){
  function normal_bind(delay){
    var next_page="sec"+(sec_num+1).toString()+".html";
    var prev_page="sec"+(sec_num-1).toString()+".html";
    $('nav .right').hide();
    $('nav .left').hide();
    setTimeout(function(){
      $('nav .right').fadeIn();
      $('nav .left').fadeIn();
      $('nav .right').click(function(){
        window.location.replace(next_page);
      });
      $('nav .left').click(function(){
        window.location.replace(prev_page);
      });

      //Keyboard 37 left 39 right
      $(document).keydown(function(e) {
      switch(e.which) {
          case 37: // left
          window.location.replace(prev_page);
          break;
          case 39: // right
          window.location.replace(next_page);
          break;
          default: return; // exit this handler for other keys
      }
      e.preventDefault(); // prevent the default action (scroll / move caret)
      });
    },delay);


  }
  function overlay_bind(){
    var next_page="sec"+(sec_num+1).toString()+".html";
    var prev_page="sec"+(sec_num-1).toString()+".html";
    $("nav .right").hide();
    $("nav .left").hide();
    setTimeout(function(){
      $("nav .right").fadeIn();
      $("nav .left").fadeIn();
      //Upon Clicking next;
      $('nav .right').click(function(){
        $('.overlay').fadeIn();
      });
      $('#next').click(function(){
        window.location.replace(next_page);
      });
      $('#close').click(function(){
          $('.overlay').fadeOut();
      });
      $('nav .left').click(function(){
        window.location.replace(prev_page);
      });

      //Keyboard 37 left 39 right
      $(document).keydown(function(e) {
      switch(e.which) {
          case 37: // left
          window.location.replace(prev_page);
          break;
          case 39: // right
          $('.overlay').fadeIn();
          break;
          default: return; // exit this handler for other keys
      }
      e.preventDefault(); // prevent the default action (scroll / move caret)
      });
    },1500);
  }


  if(sec_num==1)
    {
      $('nav .left').hide();
      $('nav .right').hide();
      $('.intro-body h1').hide();
      $('.intro-body img').hide();
      $('.intro-body h1').show().css("animation","Drop 1s ease-out");
      $('.intro-body img').show().css("animation","Drop 1s ease");
      setTimeout(function(){
        $('nav .right').fadeIn();
        $(document).keydown(function(e) {
        switch(e.which) {
            case 39: // right
            window.location.replace("pages/sec2.html");
            break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
        });
      }, 1500);

    }
  else if(sec_num==2)
    {
      $('nav .left').hide();
      $('nav .right').hide();
      $('.intro-body h1').hide();
      $('.intro-body img').hide();
      $('.intro-body h1').show().css("animation","Drop 1s ease-out");
      setTimeout(function(){
        $('#logo2 img').fadeIn();
      },1500);
      setTimeout(function(){
        $('#logo3 img').fadeIn();
      },1900);
      setTimeout(function(){
        $('#logo1 img').fadeIn();
      },2300);
      setTimeout(function(){
        $('.content').fadeIn();
      },2500);
      setTimeout(function(){
        $('nav .right').fadeIn();
        $('nav .left').fadeIn();

        //Upon Clicking next;
        $('nav .right').click(function(){
          $('.overlay').fadeIn();
        });
        $('#next').click(function(){
          window.location.replace("sec3.html");
        });
        $('#close').click(function(){
            $('.overlay').fadeOut();
        });

        //Keyboard 37 left 39 right
        $(document).keydown(function(e) {
        switch(e.which) {
            case 37: // left
            window.location.replace("../index.html");
            break;
            case 39: // right
            window.location.replace("sec3.html");
            break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
        });

      }, 3500);




    }
  else if(sec_num==3)
    {
      $('section').fadeIn(1500);
      overlay_bind();
    }
  else if(sec_num==4)
    {
      $('section').fadeIn(1500);
      normal_bind(1500);
    }
  else if(sec_num==5)
    {
      $('section').fadeIn(1500);
      normal_bind(1500);
    }
  else if(sec_num==6)
    {
      $('section').fadeIn(1500);
      overlay_bind();
    }
  else if(sec_num==7)
    {
      $('section').fadeIn(1500);
      overlay_bind(1500);
    }
  else if (sec_num==8)
    {
      $('section').fadeIn(1500);
      overlay_bind(1500);
    }
  else if (sec_num==9)
    {
      $('section').fadeIn(1500);
      normal_bind(1500);
    }


});
