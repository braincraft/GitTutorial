--------------
# Git Tutorial
--------------
  - Created by Gian Carlo B. Sagun

## Git Introduction
-----------------
### What is Git?

  **GIT** - is a widely used version control system for software development. It is a distributed revision control system with an emphasis on speed, data integrity, and support for distributed, non-linear workflows. Git was initially designed and developed by Linus Torvalds for Linux kernel development in 2005.
  (Source: [wikipedia](https://en.wikipedia.org/wiki/Git_(software))

### Why use Git?


  **Version control** and **collaboration** is much easier using git. Common problem on a team of programmers is that there is a deadlock waiting for the finished feature of a member of the team, thus it is not time-efficient and not productive at all.

  Other problems solved by git:
  * Working on the same part of the code
  * A member editted a part of the code, now no one knows how to fix it
  * Code is now totally chaotic, and files in your dropbox and facebook group is piling up, and no one knows what file is the correct version.
  * No one knows who editted that part of the code that leads to several errors and bugs.

## Git Installation
----
  It is very easy and simple to install git on any operating system. Just follow the documentation provided by the git website. [Click Here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git "Git Installation")

### SSH Keys
  These are fingerprints used by your computer to get access to a _repository_. Adding one of them and using one of them is quite easy. Here is the documentation in adding one [Click Here](https://help.github.com/articles/generating-ssh-keys/ "Adding an SSH Key").

  _If you are using Windows, I recommend openning your **git bash** and do the instructions stated in the documentation in there._

  **Note: _Using ssh keys_** are not required but very useful and convenient. Do **NOT GIVE** your ssh_keys to anyone, it is like giving your identity as a programmer.

## Learning on How to use Git
----
  Go to your **Desktop** or **Documents**
  Clone this repository using `git clone git@gitlab.com:braincraft/GitTutorial.git` on your terminal if you are on linux. If you are windows, do this on your git bash (_right click and select `Open Git Bash Here`_). Then
  **Start by opening index.html**
